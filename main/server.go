package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func homeRouterHandler(w http.ResponseWriter, r *http.Request) {
	status := "unauthorized"
	t, err := template.ParseFiles(
		"templates/index.html",
		"templates/"+status+"/header.html",
		"templates/"+status+"/main.html",
		"templates/footer.html",
	)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	t.ExecuteTemplate(w, "index", nil)
}

func loginFormRouterHandler(w http.ResponseWriter, r *http.Request) {
	status := "login"
	t, err := template.ParseFiles(
		"templates/index.html",
		"templates/"+status+"/header.html",
		"templates/"+status+"/main.html",
		"templates/footer.html",
	)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	t.ExecuteTemplate(w, "index", nil)
}

func registerFormRouterHandler(w http.ResponseWriter, r *http.Request) {
	status := "register"
	t, err := template.ParseFiles(
		"templates/index.html",
		"templates/"+status+"/header.html",
		"templates/"+status+"/main.html",
		"templates/footer.html",
	)
	if err != nil {
		fmt.Fprint(w, err.Error())
		return
	}
	t.ExecuteTemplate(w, "index", nil)
}

func authorizedRouterHandler(w http.ResponseWriter, r *http.Request) {
	status := "authorized"
	r.ParseForm()
	var login, password string
	if _, ok := r.Form["login"]; ok {
		login = r.Form["login"][0]
	}
	if _, ok := r.Form["password"]; ok {
		password = r.Form["password"][0]
	}
	err := loginCheck(login, password)
	if err != nil {
		status = "unauthorized"
	}
	t, erro := template.ParseFiles(
		"templates/index.html",
		"templates/"+status+"/header.html",
		"templates/"+status+"/main.html",
		"templates/footer.html",
	)
	if erro != nil {
		fmt.Fprint(w, erro.Error())
		return
	}
	t.ExecuteTemplate(w, "index", nil)
	if err != nil {
		fmt.Fprint(w, err.Error())
	}
}

func registerRouterHandler(w http.ResponseWriter, r *http.Request) {
	status := "authorized"
	var info string
	r.ParseForm()
	var login, password, name, age string
	if _, ok := r.Form["login"]; ok {
		login = r.Form["login"][0]
	}
	if _, ok := r.Form["password"]; ok {
		password = r.Form["password"][0]
	}
	if _, ok := r.Form["name"]; ok {
		name = r.Form["name"][0]
	}
	if _, ok := r.Form["age"]; ok {
		age = r.Form["age"][0]
	}

	err := loginCheck(login, password)
	if err == nil {
		status = "login"
		info = "Info: User found. Sign in"
	} else {
		register(login, password, name, age)
		status = "registered"
	}

	t, erro := template.ParseFiles(
		"templates/index.html",
		"templates/"+status+"/header.html",
		"templates/"+status+"/main.html",
		"templates/footer.html",
	)
	if erro != nil {
		fmt.Fprint(w, erro.Error())
		return
	}
	t.ExecuteTemplate(w, "index", nil)
	fmt.Fprint(w, info)

}

func main() {
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	http.HandleFunc("/", homeRouterHandler)
	http.HandleFunc("/register", registerFormRouterHandler)
	http.HandleFunc("/registered", registerRouterHandler)
	http.HandleFunc("/login", loginFormRouterHandler)
	http.HandleFunc("/authorized", authorizedRouterHandler)
	err := http.ListenAndServe(":9000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
