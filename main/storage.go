package main

import (
	"errors"
	"fmt"
)

type (
	// Bio struct
	Bio struct {
		Name string
		Age  string
	}
	// User struct
	User struct {
		Login    string
		Password string
		Bio
	}
	// Cache struct
	Cache map[string]User
)

var cacheByLogin Cache

func init() {
	demoUser := User{
		Login:    "user",
		Password: "user",
		Bio: Bio{
			Name: "user",
			Age:  "2",
		},
	}
	cacheByLogin = Cache{demoUser.Login: demoUser}
	fmt.Println(cacheByLogin)
}
func loginCheck(login, pass string) (err error) {
	if user, ok := cacheByLogin[login]; ok && user.Password == pass {
		return
	}
	return errors.New("Info: invalid pass or login")
}

func register(login, pass, name, age string) (err error) {
	if _, ok := cacheByLogin[login]; ok {
		return errors.New("Info: User found. Sign in")
	}
	cacheByLogin[login] = User{
		Login:    login,
		Password: pass,
		Bio: Bio{
			Name: name,
			Age:  age,
		},
	}
	return
}
